import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
  apiKey: "AIzaSyCfqu6cNmdoIBYOArkOqXlXV-oIMMwO3kg",
  authDomain: "workshop-reactapp.firebaseapp.com",
  databaseURL: "https://workshop-reactapp.firebaseio.com",
  projectId: "workshop-reactapp",
  storageBucket: "workshop-reactapp.appspot.com",
  messagingSenderId: "1098064994108"
};

firebase.initializeApp(config);

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
  database,
  auth,
  provider
}