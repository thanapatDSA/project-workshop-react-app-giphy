import React from 'react'
import { Card } from 'antd'


const { Meta } = Card
function GifItem(props) {
  return (
    
    <Card
      style={{
        height: 'auto',
        width: '440px',
        textAlign: 'center',
      }}
      bordered={true}
      onClick={() => {
        props.onItemClick(props.item)
      }}
      hoverable='false'
      cover={<img src={props.item.images.fixed_width.url}
        style={{
          height: '240px',
          width: '440px'
        }}
        alt='' />}>
      <Meta
        title={props.item.title+" "}
      />
    </Card>
  )
}

export default GifItem
