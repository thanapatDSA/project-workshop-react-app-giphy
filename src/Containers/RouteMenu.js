import React from 'react';
import ListGif from './Gif'
import FavoritemPage from './Favorite'
import Profile from './Profile'
import { Route, Switch, Redirect } from 'react-router-dom';

function RouteMenu(props) {
  return (
    <Switch>
      <Route
        path="/home"
        exact
        render={() => {
          return (
            <ListGif
              items={props.items}
              onItemClick={props.onItemClick}
              onSearchChange={props.onSearchChange}
              onSelectPage={props.onSelectPage}
            />
          )
        }}
      />
      <Route path="/favorite" exact render={() => {
        return (
          <FavoritemPage
            onItemClick={props.onItemClick}
          />
        )
      }}  />
      <Route path="/profile" exact component={Profile} />
      <Redirect from="/*" exact to="/" />
    </Switch>
  )
}

export default RouteMenu