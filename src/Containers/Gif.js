import React, { Component } from 'react'
import { Spin, Input, List, Layout } from 'antd'
import ItemGif from '../Components/gif/item'

const { Search } = Input
class GifPage extends Component {
  state = {
    items: [],
    isLoading: false,
    current: 1
  }
  toTop =(page)=>{
    window.scrollTo(0, 0)
    this.props.onSelectPage(page)
  }

  render() {
    return (
      <div
        style={{
          padding: '16px',
          minHeight: '300px',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
        }}
      >
        <Layout
          style={{
            padding: '16px',
            minHeight: '300px',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',

          }}>
          <Search
            placeholder="Search GIFs"
            enterButton="Search"
            size="large"
            onSearch={this.props.onSearchChange} />
          <Layout
            style={{
              padding: '16px',
              minHeight: '30px',
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',

            }}>

          </Layout>

          {!this.state.items.length > 0 ? (
            <List
              pagination = {{ pageSize : 40, alignItems: 'center'}}
              grid={{ gutter: 20, column: 4 }}
              dataSource={this.props.items}
              renderItem={item => (
                <List.Item>
                  <ItemGif
                    item={item}
                    onItemClick={this.props.onItemClick} />
                </List.Item>
              )}
            />
          ) : (
            <Spin size="large"/>
            )
          }
        </Layout>
      </div >
    )
  }
}
export default GifPage