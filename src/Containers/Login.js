import React, { Component } from 'react'
import { Card, message, Icon, Form, Button, Input, Layout } from 'antd'
import { auth, provider } from '../firebase'

const Background = `https://source.unsplash.com/random`
class Login extends Component {
  state = {
    isLoading: false,
    email: '',
    password: '',
    isShowModal: false,
    isLogin: false,
    imageUrl: ''
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.navigateToMainPage()
    }
  }

  navigateToMainPage = () => {
    const { history } = this.props
    history.push('/home')
  }

  saveInformationUser = email => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    )
    this.setState({ isLoading: false })
    this.navigateToMainPage()
  }

  facebookLogin = () => {
    auth.signInWithPopup(provider).then(({ user }) => {
      this.setState({ imageUrl: `${user.photoURL}?height=500` })
      this.saveInformationUser(user.email)
    })
  }

  facebookLogout = () => {
    auth.signOut().then(() => {
      this.setState({ user: null })
      message.success('Log out success')
    })
  }
  onLogin = () => {
    message.warn(<img style={{ width: 150, height: 150 }} src={'https://i.kym-cdn.com/photos/images/facebook/000/605/020/d99.jpg'} alt=''/>, 1)
  }
  render() {
    return (
      <div type="flex" style={{
        padding: '60px',
        position: 'absolute',
        width: '100%',
        height: '100%',
        minHeight: '100%',
        margin: '0 auto',
        backgroundImage: `url(${Background})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center'
      }}  >

        <Layout
          hoverable
          style={{
            padding: '100px',
            background: 'transparent',
            backgroundColor: 'rgba(0,0,0,0.6)',
            width: '100%',
            height: '100%',
          }}>
          <Form style={{ margin: '0 auto' }}>
            <Card 
            hoverable 
            bordered
            style={{ width: 'auto', height: '400px' }}>
              <img style={{ width: 'auto', height: 50 }} src={'https://static1.squarespace.com/static/598c6058cd39c3692cc5a8bd/t/5b966cc9b8a045c01ce0e55d/1536589243221/giphy.png'} alt='' />
              <br /> <br />
              <h2>LOGIN</h2>
              <Form.Item>
                <Input prefix={
                  <Icon type="user"
                    style={{
                      color: 'rgba(0,0,0,.25)'
                    }} />}
                  placeholder="Email" />
              </Form.Item>
              <Form.Item>
                <Input prefix={
                  <Icon
                    type="lock"
                    style={{
                      color: 'rgba(0,0,0,.25)'
                    }} />}
                  type="password"
                  placeholder="Password" />
              </Form.Item>
              <Form.Item>
                <Button type="primary"
                  htmlType="submit"
                  block
                  onClick={this.onLogin}>
                  Log in
          </Button>
                <Button type="primary"
                  htmlType="submit"
                  block
                  icon="facebook"
                  onClick={this.facebookLogin}>
                  Login With Facebook
          </Button>
                <p style={{ textAlign: 'center', background: 'white' }}>Giphy Application Workshop @ CAMT</p>
              </Form.Item>
            </Card>
          </Form>
        </Layout>
      </div>
    )
  }
}

export default Login